
<h2>Contact us</h2>
<!--A question? A bug? A suggestion?-->

<p>You can post a bug report on the <a href="https://gitlab.com/mwetoolkit/mwetoolkit/issues" target="_blank">Gitlab issue tracker</a> (requires a free Gitlab account).</p>

<p>You can post your message or bug report on the <a href="https://groups.google.com/d/forum/mwetoolkit" target="_blank">public mailing list</a>.</p>

<p>For specific questions, please don't hesitate to send us an email. Our username is <tt>mwetoolkit</tt> and the email provider is <strong>Google mail</strong>, can you guess our email address? ;-)</p>

<p>We always welcome new contributors. If you are looking for an internship, a software development project in Python, etc. please tell us and we can try to come up with an interesting project topic for you.</p>

<hr/>

<h3>The Toolkit Developers</h3>

<p>The <tt>mwetoolkit</tt> is developed and maintained by:</p>

<ul>
<li>Silvio Ricardo Cordeiro</li>
<li><a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch/">Carlos Ramisch</a></li>
</ul>

<h3>Previous contributors (and their contributions)</h3>

<ul>
<li>Manon Scholivet (CRF tagger)</li>
<li><a href="http://inf.ufrgs.br/~vbuaraujo/">Vitor de Araujo</a> (C indexer, tests, etc.)</li>
<li>Abdelali Dyany (GUI)</li>
<li>Joseph Jonathan (GUI)</li>
<li>Sandra Castellanos (Spanish tests, doc)</li>
<li>Maite Dupont (from_rasp.py)</li>
<li>Victor Yoshiaki Miyai (csv2xml.py)
<li><a href="http://www.veneraarnaoudova.ca/">Venera Arnaoudova</a> (eval_manual.py)</li>
</ul>
