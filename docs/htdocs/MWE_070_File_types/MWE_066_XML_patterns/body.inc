<h2>XML patterns</h2>

<!-- Short description -->
<p>A list of regex-style patterns represented in XML.</p>


<h3>Characteristics</h3>

<p>See the generic <a href="?sitesig=MWE&page=MWE_070_File_types&subpage=MWE_060_XML">XML description</a>.</p>



<h3>Details</h3>

<p>See <a href="?sitesig=MWE&page=MWE_020_Tutorials&subpage=MWE_030_Defining_Patterns">Defining Patterns</a>.</p>



<h3>Example</h3>

<p>You can have a look at a sample <a
href="https://gitlab.com/mwetoolkit/mwetoolkit/tree/master/test/filetype-samples/patterns.xml">patterns</a>
file.</p>

<hr/>

