<html>
<head>
<title>{PAGETITLE} </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
{META}
<script type="text/javascript" src="MWE_MAIN/scripts/functions.js"></script>
<style>
.info { 
  background: #f1f1f1; 
  font-weight: bold; 
}

.pre, pre { 
  background: #111; 
  color: #f1f1f1; 
  padding: 5px 8px; 
  display: block;
  font-family: monospace;
  margin: 1em 0px 1em;
}


/* Writing styles for Tools page here (doesn't work as a separate .css page) */
.tooldescr {
    text-align: left;
    background: #f0f0f0;
    padding:10px;
    margin-bottom:10px;
    font-size:14px;
    border-radius: 5px;
    width: 90%;
}
.tooldescr:hover {
    background: #d9d9d9;
}

.toolheader { cursor: pointer; }
.usage { font-family: monospace; }
.toolname { font-weight: bold; }
.shortdescr {
    margin-left: 20px;
    margin-top: 8px;
    font-style: italic;
}

.expandedinfo {
    display: none;
    margin-top: 20px;
    background-color: "#F00";
}
.subtitle {
    margin-top: 30px;
    font-weight: bold;
}
.entry{
    margin-left: 10px;
    margin-top: 20px;
}
.entry_header {
    font-family: monospace;
}
.entry_descr {
    margin-left: 10px;
}

.header_blockref, .blockref, .filetyperef {
    text-decoration: none;
    color: black;
}
.header_blockref:hover, .blockref:hover, .filetyperef:hover {
    text-decoration: underline;
}
</style>
</head>

<body bgcolor="#FFFFFF" text="#000000" link="#006699" vlink="#003366" alink="#CCCCCC" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<a href="{HOMELINK}"><img style="padding:10px 0px 0px 0px" src="./images/mwetoolkit_logo.png" width="300px" alt="{SITENAME}"/></a><hr/>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr align="left" valign="top"> 
	<td width="200" height="400" bgcolor="#FFFFFF"> <b><font face="Arial, Helvetica, sans-serif" size="3">
	<br>&nbsp;<a href="{HOMELINK}">{HOMENAME}</a><br>
<ul style="margin:5px;padding:5px 6px">	
<!-- START BLOCK : LEFT_NAV -->
      <font face="Arial, Helvetica, sans-serif" size="3">
      
	 <li style='margin:7px'><a style='text-decoration:none' href="{OPTION_LINK}">{OPTION_NAME}</a><br></font></li>
<!-- START BLOCK : SUB_NAV -->
      <font face="Arial, Helvetica, sans-serif" size="2"><nobr>&nbsp;&nbsp;&#149;&nbsp;<a style='text-decoration:none' href="{SUB_LINK}">{SUB_NAME}</a></nobr></font><br>
<!-- END BLOCK : SUB_NAV -->
<!-- END BLOCK : LEFT_NAV -->
     </ul>
<!-- START BLOCK : LEFT_BOX -->	  
	  <table width="90%" border="1" cellspacing="0" cellpadding="2" align="center">
		<tr><td bgcolor="#3399CC" align="center" valign="top"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif"> 
			{LB_TITLE}</font></td></tr>
		<tr><td><font face="Arial, Helvetica, sans-serif" size="1">{LB_CONTENT}</font></tr>
	  </table><br>
<!-- END BLOCK : LEFT_BOX -->
	  <br>
	</td>
	<td bgcolor="#FFFFFF"> 

<div style="font-family:Verdana,Arial;font-size:100%;margin:5px;text-align:justify">
{BODY}
</div>

<!-- START BLOCK : CENTER_BOX -->
<p><hr><br><strong>{CENTER_TITLE}</strong><br>
{CENTER_CONTENT}
<!-- END BLOCK : CENTER_BOX -->

	</td>
	<td width="200" bgcolor="#FFFFFF"> 
<!-- START BLOCK : RIGHT_BOX -->
	  <table width="200" border="0" cellspacing="0" cellpadding="1">
		<tr> 
		  <td bgcolor="#006699" align="center" valign="top"><b><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"> 
			<style type="text/css">
				li {margin: 10px}
			</style>
			{RB_TITLE}</font> </b> 
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
			  <tr> 
				<td bgcolor="#FFFFFF"> 
                  {RB_CONTENT}
				</td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table><br>
<!-- END BLOCK : RIGHT_BOX -->
	</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr bgcolor="#006699"> 
	<td width="110">&nbsp;</td>
	<td align="center" valign="middle"><font color="#FFFFFF" size="1" face="Arial, Helvetica, sans-serif">
	{FOOTER}</font></td>
	<td width="200">&nbsp;</td>
  </tr>
</table>
</body>
</html>
