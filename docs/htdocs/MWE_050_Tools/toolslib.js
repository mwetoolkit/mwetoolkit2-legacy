// Set to display only elements of class `tooldescr`
// whose content matches `regex_string`.
function jsFilterTool(regex_string) {
    patterns = regex_string.split(" ")
    for (var p=0; p < patterns.length;  ++p) {
        patterns[p] = new RegExp(patterns[p], "im")
    }

    var entries = document.getElementsByClassName("tooldescr");
    for (var e=0; e < entries.length; ++e) {
        var target_data = entries[e].textContent;
        var good = true;

        for (var p=0;  p < patterns.length; ++p) {
            if (!patterns[p].test(target_data)) {
                good = false; break;
            }
        }
        if (good) {
            entries[e].style.display = "block";
        } else {
            entries[e].style.display = "none";
        }
    }
}


// Expand HTML information inside `tooldescr_obj`
function jsExpandToolInfo(tooldescr_obj, callback) {
    if (!callback) callback = function() {}
    jsLoadToolInfo(tooldescr_obj, function(toolblock) {
        if (toolblock.style.display == "block") {  // if currently displayed
            toolblock.style.display = "none";
        } else {
            toolblock.style.display = "block";
        }
        callback(toolblock);
    });
}


// Load HTML information inside `tooldescr_obj`
function jsLoadToolInfo(tooldescr_obj, callback) {
    if (!callback) callback = function() {}
    toolname = tooldescr_obj.getElementsByClassName("toolname")[0];
    toolpath = "MWE_050_Tools/toolblocks/" + toolname.textContent;

    jsLoadFile(toolpath, function(content) {
        toolblock = tooldescr_obj.getElementsByClassName("expandedinfo")[0];
        toolblock.innerHTML = content;
        callback(toolblock);
    });
}


// Load file and pass contents to `on_success`
function jsLoadFile(filepath, on_success) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            on_success(xhttp.responseText)
        }
    }
    xhttp.open("GET", filepath, true);
    xhttp.send();
}


// Called after loading Tools page
function jsLoadedPage() {
    // 1) Focuses the 'filter button'
    var cur_hash = window.location.hash.substring(1);
    var autofocusedFilterButton = document.getElementById("autofocusedFilterButton");
    autofocusedFilterButton.focus()  // we do not use <input autofocus/>, as it is buggy

    // 2) Opens and scrolls into '#section', if requested in URL
    if (cur_hash != "") {
        // cur_hash may be something like "grep.py/--from"
        var tool_elem = document.getElementById(cur_hash.split("/")[0]);
        var toolheader = tool_elem.getElementsByClassName("toolheader")[0];
        jsExpandToolInfo(tool_elem, function(_) {
            jsGoTo(cur_hash);
        });
    }

    // 3) Set up magical links for `header_blocref`s
    var header_blocrefs = document.getElementsByClassName("header_blockref");
    for (var i=0;  i < header_blocrefs.length;  ++i) {
        header_blocrefs[i].setAttribute("onclick", "jsClickedHeaderBlockref(event, this);");
    }
}


// Called when clicking on a `header_blocref`
function jsClickedHeaderBlockref(event, header_blockref) {
    event.stopPropagation();
    toolname = header_blockref.href.split("#")[1].split("/")[0];
    var tooldescr_obj = document.getElementById(toolname);
    jsLoadToolInfo(tooldescr_obj, function(toolblock) {
        toolblock.style.display = "block";
        jsGoTo(header_blockref.href.split("#")[1]);
    });
}

// Go to reference, e.g. "grep.py/--from"
function jsGoTo(href) {
    var selection = document.getElementById(href);
    if (selection) {
        selection.scrollIntoView();
    }
}
