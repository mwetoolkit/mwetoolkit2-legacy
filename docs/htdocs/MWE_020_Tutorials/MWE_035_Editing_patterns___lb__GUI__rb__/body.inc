
<h2>Getting started with MWE pattern designer</h2>

<p>The mwetoolkit extracts MWE candidates by matching each sentence in the corpus against a set of patterns specified by the user. These patterns are read from XML files. This document describes how to create such files without knowing anything about XML. For more information about patterns you can visit this page: <a href="?sitesig=MWE&page=MWE_020_Quick_Start&subpage=MWE_030_Defining_Patterns">Defining patterns for extraction</a>.</p>

<p>MWE patterns designer is written in <span class="info">python</span> using <span class="info">wxPython</span>.</p>

<h3	>Requirements</h3>
<ul>
	<li>python 2.7+</li>
	<li><a href="http://wiki.wxpython.org/How%20to%20install%20wxPython#Install_Python">wxPython</a></li>
</ul>

<h3	>Usage</h3>
<p>Inside the mwetoolkit folder, navigate to the <span class="info">gui</span> folder:</p>
<pre>$ cd gui</pre>
<p>Then launch the application:</p>
<pre>$ make</pre>
<p>We include a Makefile as a shorcut command. If you don't have <span class="info">Makefile</span> installed, run:</p>
<pre>$ python MWEApp.py</pre>

<h3>Example 1: verb + noun + adjective expressions</h3>

<p>Let's say that we want to extract expressions of the form <span class="info">verb + noun + adjective</span> from the following corpus.</p>

<p>When you start the application, this is what you should see.</p>
<p>On the left hand side, we have the tree representation of the patterns. At the beginning, we just have the root element named <span class="info">patterns</span>.</p>
<p>Right click on <span class="info">patterns</span> to add a sequence by choosing <span class="info">Add a sequence</span>.</p>
<img src="images/example1_1.png" />

<p>Underneath <span class="info">patterns</span>, <span class="info">sequence</span> has been added. If you select <span class="info">sequence</span> you should see a panel on the right hand side to edit its attributes. For now, we don't need to edit anything. Let's right click on <span class="info">sequence</span> and choose <span class="info">Add a word pattern</span>.</p>
<img src="images/example1_2.png" />

<p>Because we want to extract expressions of the form <span class="info">verb + noun + adjective</span>, let's add two other word pattern.</p>
<p>We now have 3 words patterns, but they are empty. We need to specify that the first word is a <span class="info">verb</span>, the second word is a <span class="info">noun</span> and the third word is an <span class="info">adjective</span>. To do that, add a new element by clicking the <span class="info">+</span> button.</p>
<img src="images/example1_3.png" />

<p>A dialog window will pop-up. Select the <span class="info">pos</span> (part of speech) attribute and type <span class="info">VER*</span> for the value and click OK.</p>
<img src="images/example1_4.png" />
<br />
<img src="images/example1_5.png" />

<p>For the second word pattern, add a new element, select the <span class="info">pos</span> attribute and type <span class="info">NOM</span> for the value and valid by clicking the OK button.</p>
<img src="images/example1_6.png" />
<p>For the third word pattern, do the same thing but type <span class="info">ADJ</span> as the value.</p>
<img src="images/example1_7.png" />

<p>The values <span class="info">VER*</span> (because we want all type of verbs, we typed <span class="info">VERB</span> with an asterisk.), <span class="info">NOM</span> and <span class="info">ADJ</span> can be found by checking the corpus file.</p>

<p>To save this pattern to a file. <span class="info">File > Save as XML</span>.</p>

<h3>Example 2: verb + noun + adjective expressions</h3>

<p>Before starting this second example, let's activate the Live XML feature (<span class="info">View > Show live XML</span>) that allows you to keep an eye on the XML while creating your patterns.</p>

<p>We didn't see all the possibilities with the previous example, so let's try another one. For the second example, we want to extract expressions of the form:
<ul>
    <li>either of the following words</li>
    <ul>
        <li>verb</li>
        <li>adjective</li>
        <li>preposition</li>
    </ul>
    <li>noun</li>
</ul>
For the verbs, we will specify that we don't want past tense verbs.
</p>

<p>Right click on <span class="info">patterns</span> to add a sequence by choosing <span class="info">Add a sequence</span>.</p>
<img src="images/example2_1.png" />

<p>Right click on <span class="info">sequence</span> and choose <span class="info">Add an either pattern</span> to represent either a <span class="info">verb</span>, or an <span class="info">adjective</span> or a <span class="info">preposition</span>. Right click again on <span class="info">sequence</span> and choose <span class="info">Add a word pattern</span> to represent <span class="info">noun</span> words.</p>

<p>We added the either pattern, but we did not add the representation of a <span class="info">verb</span> nor an <span class="info">adjective</span> nor a <span class="info">verb</span> yet. Let's do that.</p>
<p>Right click on <span class="info">either</span> and choose <span class="info">Add a sequence pattern</span>. Now right click on <span class="info">sequence</span> and choose <span class="info">Add a word pattern</span>, this one will represent the <span class="info">verbs</span>. Right click a second time on <span class="info">sequence</span> and choose again <span class="info">Add a word pattern</span> to represent the <span class="info">adjectives</span>. To represent the <span class="info">prepositions</span> we need to add a third word pattern. Right click on <span class="info">sequence</span> choose <span class="info">Add a word pattern</span>.</p>
<img src="images/example2_2.png" />

<p>We now have our base but we need to specify that the first word is a <span class="info">verb</span>, etc ... </p>
<p>Select the first <span class="info">word</span> inside the <span class="info">either</span>, add a new element by clicking the <span class="info">+</span> button. Select the <span class="info">pos</span> for the attribute and type <span class="info">VER*</span> for the value and click OK. Because we don't want past-tense verbs, we need to add a new element. Click the <span class="info">+</span> button. Select the <span class="info">pos</span> for the attribute and type <span class="info">VER:???</span> for the value, check the <span class="info">negative</span> checkbox and click <span class="info">OK</span>.</p>
<p>Select the second <span class="info">word</span> inside the <span class="info">either</span>, add a new element by clicking the <span class="info">+</span> button. Select the <span class="info">pos</span> for the attribute and type <span class="info">ADJ</span> for the value and click OK.</p>
<p>Select the third <span class="info">word</span> inside the <span class="info">either</span>, add a new element by clicking the <span class="info">+</span> button. Select the <span class="info">pos</span> for the attribute and type <span class="info">PRP</span> for the value and click OK.</p>
<p>Select the last <span class="info">word</span> (which is the second element of <span class="info">patterns > sequence</span>), add a new element by clicking the <span class="info">+</span> button. Select the <span class="info">pos</span> for the attribute and type <span class="info">PRP</span> for the value and click OK.</p>

<p>To save this pattern to a file. <span class="info">File > Save as XML</span>.</p>

