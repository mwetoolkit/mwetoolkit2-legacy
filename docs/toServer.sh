#! /bin/bash
HERE="$(cd "$(dirname "$0")" && pwd)"

set -o nounset    # Using "$UNDEF" var raises error
set -o errexit    # Exit on error, do not continue quietly


usage() {
    echo "Usage: $(basename "$0") <sourceforge-username>"
    echo "Build website and send it to HTTP server."
}
fail() {
    echo "$(basename "$0"): $1"; exit 1
}

# Handle optional args
while getopts :"h" FLAG; do
    case "$FLAG" in
        h)  usage; exit 0 ;;
        :)  fail "missing arg to -$OPTARG" ;;
        \?) fail "bad flag: -$OPTARG" ;;
        *)  fail "NOT IMPLEMENTED: -$FLAG" ;;
    esac
done
shift "$((OPTIND - 1))"

# Handle positional args
test "$#" -ne 1  && fail "expected 1 positional arg"
username="$1"; shift

############################################################


cd "$HERE"

../meta/make_html_tools.py

echo "Warning: not showing directory permission modifications"
rsync -rlptC --stats --compress --del --progress -i htdocs ${username},mwetoolkit@web.sf.net: | sed '/^\.d\.\.\.p\.\.\.\.\./d'
