#! /usr/bin/env python

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

__all__ = [
    "parser", "Main"
]

import argparse
import codecs
import io
import os
import re
import sys
import shutil
import sys
import textwrap

import metainfo


parser = argparse.ArgumentParser(add_help=False, description="""
        Build HTML (.inc) files at MWETKDIR/docs/htdocs/MWE_050_Tools""")
parser.add_argument("-h", "--help", action="help",
        help=argparse.SUPPRESS)

TOOLSDIR = os.path.join(metainfo.HERE, "../docs/htdocs/MWE_050_Tools")
TOOLBLOCKSDIR = os.path.join(TOOLSDIR, "toolblocks")

RE_REF_FLAG = re.compile(r'(--\w+(-\w+)*)')
RE_REF_FTYPE = re.compile(r'"(\w+)"')


class Main(object):
    def __init__(self, args):
        self.args = args

    def run(self):
        self.ft2docs = metainfo.calculate_filetype2docspath()
        if not self.ft2docs:  raise Exception("Bug in filetypes' HTML")
        print("==> File types found:", " ".join(self.ft2docs.keys()))
        print("="*50)  # end of setup

        self.bodyinc = io.open(os.path.join(TOOLSDIR, "body.inc"), "w+")
        shutil.rmtree(TOOLBLOCKSDIR, ignore_errors=True)
        os.makedirs(TOOLBLOCKSDIR)

        self.generate(self.bodyinc, """
            <!--\n\n\n\n\n
                DO NOT WRITE TO THIS HTML FILE.
                This file has been automatically generated.
                You should modify `{filename}` instead.
                \n\n\n\n\n
            -->

            <h2>Tools</h2>

            <script language="javascript"
                    src="MWE_050_Tools/toolslib.js"></script>

            <div style="margin-bottom:12px">
                <input id="autofocusedFilterButton"
                        oninput="jsFilterTool(this.value)"
                        placeholder="Filter tools..." />
            </div>
        """.format(filename=os.path.basename(sys.argv[0])))

        for toolpath in metainfo.toolpaths():
            print("=> Parsing:", os.path.basename(toolpath))
            if toolpath.endswith(".py"):  # TODO REMOVE THIS RESTRICTION
                usagetxt = metainfo.get_usage(toolpath)
                paragraphs = metainfo.usage_paragraphs(usagetxt)
                toolargs_obj = metainfo.ParsedToolArgs()
                toolargs_obj.parse(paragraphs)
                self.handle_bodyinc(toolpath, toolargs_obj)
                self.handle_toolblock(toolpath, toolargs_obj)


        self.generate(self.bodyinc, """
            <script language="javascript">jsLoadedPage();</script>
        """.format(filename=os.path.basename(sys.argv[0])))


    def handle_bodyinc(self, toolpath, toolargs_obj):
        r"""Write file summary to `body.inc`."""
        self.generate(self.bodyinc, """\n\n
            <div class="tooldescr" id="{tool_id}">
                <div class="toolheader" onclick="jsExpandToolInfo(this.parentNode, null)">
                    <div class="usage">
                        <span class="toolname">{toolname}</span> {toolargs}
                    </div>
                    <div class="shortdescr">
                        {shortdescr}
                    </div>
                </div>
                <div class="expandedinfo"></div>
            </div>
        """.format(
            tool_id=toolargs_obj.toolname,
            toolname=metainfo.html_escape(self.nonempty("toolname",
                    toolargs_obj.toolname) or os.path.basename(toolpath)),
            toolargs=" ".join(x.to_html_header(toolargs_obj.toolname) \
                    for x in toolargs_obj.required),
            shortdescr=self.nonempty("shortdescr", metainfo.html_escape(
                    toolargs_obj.header_shortdescr))
        ))


    def handle_toolblock(self, toolpath, toolargs_obj):
        r"""Write file summary to `TOOLBLOCKSDIR/TOOLNAME`."""
        toolname = os.path.basename(toolpath)
        with io.open(os.path.join(TOOLBLOCKSDIR, toolname), "w+") as toolblk:
            for extradescr in toolargs_obj.header_extradescrs:
                self.generate(toolblk, """
                    <div class="entry">
                        <div class="entry_descr">{extradescr}</div>
                    </div>
                """.format(
                    extradescr=metainfo.html_escape(extradescr)
                ))

            if toolargs_obj.required:
                self.generate(toolblk, '<div class="subtitle">Required arguments:</div>')
            for arg in toolargs_obj.required:
                self.generate_block(toolblk, toolname, arg)

            self.generate(toolblk, '<div class="subtitle">Optional arguments:</div>')
            for arg in toolargs_obj.optional:
                self.generate_block(toolblk, toolname, arg)


    def generate_block(self, toolblk, toolname, arg):
        if isinstance(arg, metainfo.ChoiceArg):
            for choice in arg.choices:
                self.generate_block(toolblk, toolname, choice)
            return

        human_descr = metainfo.html_escape(arg.human_descr)
        # Add links for filetypes (e.g. `"TreeTagger"`)
        human_descr = RE_REF_FTYPE.sub(self.sub_ftype, human_descr)
        # Add links for flags (e.g. `--from`)
        human_descr = RE_REF_FLAG.sub(
                r'<a class="blockref" href="#{toolname}/\1">\1</a>' \
                .format(toolname=toolname), human_descr)

        self.generate(toolblk, """
            <div class="entry" id="{tool_entry_id}">
                <div class="entry_header">{flag_header}</div>
                <div class="entry_descr">{human_descr}</div>
            </div>
        """.format(
            flag_header=arg.to_html_block(),
            tool_entry_id=metainfo.html_escape(toolname + "/" + arg.arg_id()),
            human_descr=human_descr,
        ))

    def sub_ftype(self, match_obj):
        r"""(Called when seeing a `"FileType"` substring. Adds a link.)"""
        path = self.ft2docs.get(match_obj.group(1))
        if path:
            return r'<a class="filetyperef" href="{path}">{ftype}</a>'.format(
                    path=metainfo.path2phite(path), ftype=match_obj.group(0))
        return match_obj.group(0)


    def generate(self, target_file, string):
        r"""Print input `string` into `target_file`."""
        if string.startswith("\n"): string = string[1:]
        if string.endswith("\n"): string = string[:-1]
        print(textwrap.dedent(string), end="", file=target_file)

    def nonempty(self, warn_identifier, string):
        r"""Check if string is non-empty and return it."""
        if not string:
            print("WARNING: got an empty value for: `{id}`".format(
                    id=warn_identifier), file=sys.stderr)
            return ""
        return string



#####################################################

if __name__ == "__main__":
    Main(parser.parse_args()).run()
